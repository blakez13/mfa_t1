import os
import sys
import math
import time
import torch
import glob
import numpy as np
import losses
import torch.nn as nn
import subprocess as sp
import torch.optim as optim
import argparse
import numpy as np
from models import unet_model
# import SimpleITK as sitk
# import torch.nn.functional as F

# import matplotlib
# from VNet.vNetModel import vnet_model
# from collections import OrderedDict
# import torch.multiprocessing
# torch.multiprocessing.set_sharing_strategy('file_system')

from scipy.io import loadmat, savemat
from types import SimpleNamespace
from skimage.restoration import denoise_tv_chambolle
from skimage.transform import hough_circle, hough_circle_peaks
from skimage.feature import canny
from skimage.draw import circle_perimeter
from scipy.ndimage.morphology import *
# from math import floor
# from VNet.losses import dice_loss
from dataset import TrainDataset, EvalDataset
from learningrate import CyclicLR
# from VNet.data_prep import generate_input_block
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler


class Sample:
    def __init__(self, sos1, sos2):

        self.ims1 = sos1.ims.clone()
        self.ims2 = sos2.ims.clone()
        if (sos1.mask - sos2.mask).sum().item() != 0:
            raise Exception('Masks are not the same.')

        if (sos1.label - sos2.label).sum().item() != 0:
            raise Exception('Labels are not the same.')

        if sos1.echo != sos2.echo:
            raise Exception('Echos are not the same.')

        if sos1.fa == sos2.fa:
            raise Exception('Flip angles are the same.')

        self.mask = sos1.mask
        self.label = sos1.label
        self.fa1 = sos1.fa
        self.fa2 = sos2.fa
        self.tr = sos1.tr
        self.echo = sos1.echo

    def get_fas(self):
        return tuple([self.fa1, self.fa2])


class T1Calc(nn.Module):
    def __init__(self, data_samples, b1, eps=0.001):
        super(T1Calc, self).__init__()

        fa1s = []
        fa2s = []
        ims1s = []
        ims2s = []

        for samp in data_samples:
            fa1s.append(samp.fa1 * math.pi / 180)
            fa2s.append(samp.fa2 * math.pi / 180)
            ims1s.append(samp.ims1[0].squeeze())
            ims2s.append(samp.ims2[0].squeeze())

        self.fa1 = torch.tensor(fa1s).view(30, 1, 1)
        self.fa2 = torch.tensor(fa2s).view(30, 1, 1)
        self.ims1 = torch.stack(ims1s, dim=0)
        self.ims2 = torch.stack(ims2s, dim=0)

        # self.ims1 = data_sample.ims1[0].squeeze()
        # self.ims2 = data_sample.ims2[0].squeeze()
        # self.fa1 = data_sample.fa1 * math.pi / 180
        # self.fa2 = data_sample.fa2 * math.pi / 180
        self.tr = data_samples[0].tr
        self.b1 = b1
        self.eps = eps

    def forward(self):

        sin1 = torch.abs(self.ims1) / torch.sin(self.fa1 * self.b1)
        sin2 = torch.abs(self.ims2) / torch.sin(self.fa2 * self.b1)

        tan1 = torch.abs(self.ims1) / torch.tan(self.fa1 * self.b1)
        tan2 = torch.abs(self.ims2) / torch.tan(self.fa2 * self.b1)

        num = sin2 - sin1
        den = tan2 - tan1

        den[(den <= self.eps) & (den >= -self.eps)] = self.eps
        num[(den <= self.eps) & (den >= -self.eps)] = self.eps

        slope = torch.clamp(num / den, self.eps, 1.1)

        log_t = torch.log(slope)
        log_t[(log_t <= 0 + self.eps) & (log_t >= 0 - self.eps)] = self.eps

        t1 = (-self.tr) / log_t

        return t1


def calc_b1_corr_t1(data_sample, b1, eps=0.001):
    #
    # fa1s = []
    # fa2s = []
    # ims1s = []
    # ims2s = []
    #
    # for samp in data_sample:
    #     fa1s.append(samp.fa1 * math.pi / 180)
    #     fa2s.append(samp.fa2 * math.pi / 180)
    #     ims1s.append(samp.ims1[0].squeeze())
    #     ims2s.append(samp.ims2[0].squeeze())
    #
    # fa1 = torch.tensor(fa1s).view(30, 1, 1)
    # fa2 = torch.tensor(fa2s).view(30, 1, 1)
    # ims1 = torch.stack(ims1s, dim=0)
    # ims2 = torch.stack(ims2s, dim=0)

    fa1 = data_sample.fa1 * math.pi / 180
    fa2 = data_sample.fa2 * math.pi / 180
    ims1 = data_sample.ims1[0].squeeze()
    ims2 = data_sample.ims2[0].squeeze()
    tr = data_sample.tr

    sin1 = torch.abs(ims1) / torch.sin(fa1 * b1)
    sin2 = torch.abs(ims2) / torch.sin(fa2 * b1)

    tan1 = torch.abs(ims1) / torch.tan(fa1 * b1)
    tan2 = torch.abs(ims2) / torch.tan(fa2 * b1)

    num = sin2 - sin1
    den = tan2 - tan1

    den[(den <= eps) & (den >= -eps)] = eps
    num[(den <= eps) & (den >= -eps)] = eps

    slope = torch.clamp(num / den, eps, 1.1)

    log_t = torch.log(slope)
    log_t[(log_t <= 0 + eps) & (log_t >= 0 - eps)] = eps

    t1 = (-tr) / log_t

    return t1


def main(opt):
    import matplotlib
    matplotlib.use('qt5agg')
    import matplotlib.pyplot as plt
    plt.ion()

    # Seed anything random to be generated
    torch.manual_seed(892)
    device = torch.device("cpu")

    # Load the data
    print('===> Loading Data ... ', end='')
    infer_samples = torch.load(f'{opt.dataDirectory}/infer_objects.pt')
    print(' done')

    same_list = infer_samples[450:480]
    size = same_list[0].ims1.shape[1:]

    # test = calc_b1_corr_t1(same_list, b1=torch.ones(size))

    # sample_num = 200
    b1 = torch.ones([1] + list(size), requires_grad=True)

    model = T1Calc(same_list, b1)
    # model = model.to(device=device)

    optimizer = optim.Adam([{'params': model.b1, 'lr': opt.lr}], weight_decay=1e-6)

    crit = nn.L1Loss()
    mask = same_list[0].mask.unsqueeze(0).to(device=device, dtype=torch.bool)
    label = same_list[0].label.unsqueeze(0).to(device=device)

    print("===> Beginning Estimating")

    epochs = range(1, opt.nEpochs + 1)

    for epoch in epochs:

        optimizer.zero_grad()
        calc_t1 = model()

        loss = crit(calc_t1, label)
        loss.backward()

        optimizer.step()
        print(f"=> Done with {epoch}. Loss: {loss.item():.02f}")

    print('Done')

    b1_map = model.b1.detach().clone()
    calc_t1 = calc_t1.detach().clone()

    # Find similar slices and see if the B1 map works
    # same_samps = []
    # sample_nums = []
    # for samp in infer_samples:
    #     if samp.mask.sum() == mask.sum().cpu():
    #         same_samps.append(samp)
    #         sample_nums.append(infer_samples.index(samp))
    #     else:
    #         pass

    diffs = []
    calc_t1s = []
    fa1s = []
    fa2s = []
    for samp in same_list:
        t1 = calc_b1_corr_t1(samp, b1_map)
        calc_t1s.append(t1)
        fa1s.append(samp.fa1)
        fa2s.append(samp.fa2)
        diffs.append(t1 - samp.label)

    for i in range(0, len(same_list)):
        fig_dir = f'./Output/figures/B1MappingSum/'
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)

        save_fig = True
        # samp = infer_samples[sample_num]
        t_fa1 = int(np.round(same_list[i].fa1))
        t_fa2 = int(np.round(same_list[i].fa2))

        plt.figure()
        plt.imshow(same_list[i].label.squeeze(), vmin=0.0, vmax=2.0, cmap='plasma')
        plt.colorbar()
        plt.axis('off')
        plt.title('STIR')
        if save_fig:
            plt.savefig(f'{fig_dir}/STIR_{i}_{t_fa1}_{t_fa2}.png',
                        dpi=300, bbox_inches='tight', pad_inches=0)

        plt.figure()
        plt.imshow(calc_t1s[i].squeeze(), vmin=0.0, vmax=2.0, cmap='plasma')
        plt.colorbar()
        plt.axis('off')
        plt.title('B1 Corrected T1')
        if save_fig:
            plt.savefig(f'{fig_dir}/B1_Corr_T1_{i}_{t_fa1}_{t_fa2}.png',
                        dpi=300, bbox_inches='tight', pad_inches=0)

        plt.figure()
        plt.imshow((calc_t1s[i] - same_list[i].label).squeeze(), vmin=-0.3, vmax=0.3, cmap='plasma')
        plt.colorbar()
        plt.axis('off')
        plt.title('B1 Corrected T1 - STIR')
        if save_fig:
            plt.savefig(f'{fig_dir}/Diff_{i}_{t_fa1}_{t_fa2}.png',
                        dpi=300, bbox_inches='tight', pad_inches=0)

        plt.figure()
        plt.imshow(b1_map.squeeze(), vmin=0.0, vmax=2.5, cmap='viridis')
        plt.colorbar()
        plt.axis('off')
        plt.title('"Ideal" B1 Map')
        if save_fig:
            plt.savefig(f'{fig_dir}/B1_Map_{i}_{t_fa1}_{t_fa2}.png',
                        dpi=300, bbox_inches='tight', pad_inches=0)

        plt.close('all')


if __name__ == '__main__':

    evalOpt = {'nEpochs': 300,
               'dataDirectory': './Data/PreProcessedData/VolumeDataPhase/',
               'model_dir': './Output/saves/',
               'outDirectory': './Output/Predictions/',
               'cuda': True,
               'threads': 0,
               'ckpt': '2020-05-21-171827/epoch_00040_model.pth',
               'crop': 128,
               'im_size': 256,
               'lr': 0.01
               }

    evalOpt = SimpleNamespace(**evalOpt)

    main(evalOpt)
