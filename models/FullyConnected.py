import torch.nn as nn


def init_weights(m):
    if isinstance(m, nn.Conv2d):
        nn.init.uniform_(m.weight.data, a=0.0, b=1.0)
        nn.init.uniform_(m.bias.data, a=0.0, b=1.0)


class FC5(nn.Module):
    """ Fully Connected 3x3 patches model"""

    def __init__(self, in_ch, out_ch):
        super(FC5, self).__init__()
        self.n1 = nn.BatchNorm2d(256)
        self.n2 = nn.BatchNorm2d(512)
        self.n3 = nn.BatchNorm2d(1024)
        self.l1 = nn.Conv2d(in_ch, 256, 5, padding=2)
        self.l2 = nn.Conv2d(256, 512, 1)
        self.l3 = nn.Conv2d(512, 1024, 1)
        self.l4 = nn.Conv2d(1024, out_ch, 1)
        self.act = nn.LeakyReLU()

    def forward(self, x):

        x = self.act(self.n1(self.l1(x)))
        x = self.act(self.n2(self.l2(x)))
        x = self.act(self.n3(self.l3(x)))
        x = self.l4(x)

        return x
