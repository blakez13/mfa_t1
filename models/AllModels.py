from .UNet.unet_parts import *


class UNetLinear(nn.Module):
    def __init__(self, n_channels, n_classes, patch=7):
        super(UNetLinear, self).__init__()
        self.inc = inconv(n_channels, 8)
        self.down1 = down(8, 16)
        self.down2 = down(16, 32)
        self.down3 = down(32, 64)
        self.down4 = down(64, 64)
        self.up1 = up(128, 32)
        self.up2 = up(64, 16)
        self.up3 = up(32, 8)
        self.up4 = up(16, 8)
        self.outc = outconv(8, 1)
        self.linear1 = nn.Conv2d(n_channels + 1, 32, 5, padding=2)
        self.linear2 = nn.Conv2d(32, 64, 1)
        self.linear3 = nn.Conv2d(64, 128, 1)
        self.linear4 = nn.Conv2d(128, n_classes, 1)

    def forward(self, locs):
        x1 = self.inc(locs)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        x = self.outc(x)
        x = self.linear1(torch.cat((x, locs), dim=1))
        x = self.linear2(x)
        x = self.linear3(x)
        x = self.linear4(x)
        return x


class FullyConnected2D(nn.Module):
    """ Fully Connected 2D patches model"""

    def __init__(self, in_ch, out_ch, patch=7):
        super(FullyConnected2D, self).__init__()
        padding = patch // 2
        self.n1 = nn.BatchNorm2d(256)
        self.n2 = nn.BatchNorm2d(512)
        self.n3 = nn.BatchNorm2d(1024)
        self.l1 = nn.Conv2d(in_ch, 256, patch, padding=padding)
        self.l2 = nn.Conv2d(256, 512, 1)
        self.l3 = nn.Conv2d(512, 1024, 1)
        self.l4 = nn.Conv2d(1024, out_ch, 1)
        self.act = nn.LeakyReLU()

    def forward(self, x):

        x = self.act(self.n1(self.l1(x)))
        x = self.act(self.n2(self.l2(x)))
        x = self.act(self.n3(self.l3(x)))
        x = self.l4(x)

        return x


class FullyConnected3D(nn.Module):
    """ Fully Connected 3D patches model"""

    def __init__(self, in_ch, out_ch, patch=7):
        super(FullyConnected3D, self).__init__()
        p = patch // 2
        padding = (p, p, 0)
        self.n1 = nn.BatchNorm3d(128)
        self.n2 = nn.BatchNorm3d(256)
        self.n3 = nn.BatchNorm3d(512)
        self.l1 = nn.Conv3d(in_ch, 128, patch, padding=padding)
        self.l2 = nn.Conv3d(128, 256, 1)
        self.l3 = nn.Conv3d(256, 512, 1)
        self.l4 = nn.Conv3d(512, out_ch, 1)
        self.act = nn.LeakyReLU()

    def forward(self, x):

        x = self.act(self.n1(self.l1(x)))
        x = self.act(self.n2(self.l2(x)))
        x = self.act(self.n3(self.l3(x)))
        x = self.l4(x)

        return x
