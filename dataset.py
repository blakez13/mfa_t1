import math
import copy
import torch
import torch.nn as nn
import kornia.augmentation as ka
import torch.utils.data as data
import torch.nn.functional as F
import torchvision.transforms.functional as TF

from itertools import product
from PIL.Image import BILINEAR


class TrainDataset(data.Dataset):

    def __init__(self, objs, opt):
        super(TrainDataset, self).__init__()

        permutes = []
        for o in objs:
            ref_objs = [x for x in o if x.fa in opt.ref_angles]
            dyn_objs = [x for x in o if x.fa in opt.dyn_angles]
            permutes += list(product(ref_objs, dyn_objs))

        self.objs = copy.deepcopy(permutes)
        self.replication = opt.repeat_factor
        self.length = len(permutes) * self.replication

        self.ref_angles = opt.ref_angles
        self.dyn_angles = opt.dyn_angles

        self.batch_size = opt.trainBatchSize

        # Need to keep track of the dimension
        self.dim = 2

        self.output = opt.label
        self.inputs = opt.inputs
        self.patch = opt.patch
        self.threeD = opt.threeD
        self.transform = opt.transform
        self.spatial = nn.Sequential(
            ka.RandomAffine(360, translate=(0.2, 0.2), scale=(0.8, 1.2), shear=(0.1, 0.1),
                            same_on_batch=True),
            ka.RandomVerticalFlip(same_on_batch=True),
            ka.RandomHorizontalFlip(same_on_batch=True)
        )

        self.input_features = self.calculate_num_input_feats()

    def get_label(self, ref_obj, slicer):

        if self.output == 'T1' or self.output == 'B1':
            return torch.clamp(ref_obj.label, 0.001, 5.0)[slicer[1:]].squeeze()

        elif self.output == 'Slope':
            stir = torch.clamp(ref_obj.label, 0.001, 5.0)
            return (torch.exp(-ref_obj.tr / stir) ** 30)[slicer[1:]].squeeze()
        else:
            exit('Unknown Label Type. Please select from [T1, B1, Slope]')

    @staticmethod
    def get_mask(ref_obj, slicer):
        return ref_obj.mask[slicer[1:]].squeeze()

    def get_inputs(self, ref_obj, dyn_obj, slicer):

        mean_ims = torch.cat([ref_obj.mean[slicer], dyn_obj.mean[slicer]], dim=0).mean(dim=0).squeeze().unsqueeze(0)
        ref_ims = ref_obj.ims[slicer].squeeze()
        dyn_ims = dyn_obj.ims[slicer].squeeze()

        input_image_list = []
        if 'Ims' in self.inputs:
            b0 = ref_obj.b0[slicer[1:]].squeeze().unsqueeze(0)
            b0 = (b0 - b0.min()) / (b0.max() - b0.min())
            input_image_list += [ref_ims / mean_ims, dyn_ims / mean_ims, b0]

        if 'Scales' in self.inputs:
            sin1 = ref_ims / torch.sin(torch.tensor(ref_obj.fa) * (math.pi / 180))
            sin2 = dyn_ims / torch.sin(torch.tensor(dyn_obj.fa) * (math.pi / 180))
            tan1 = ref_ims / torch.tan(torch.tensor(ref_obj.fa) * (math.pi / 180))
            tan2 = dyn_ims / torch.tan(torch.tensor(dyn_obj.fa) * (math.pi / 180))
            div1 = sin1 / sin2
            div2 = sin2 / sin1
            log1 = torch.log(torch.clamp(torch.abs(tan2 - tan1), min=0.001))
            input_image_list += [div1, div2, log1, torch.log(sin1), torch.log(sin2)]

        if 'FAs' in self.inputs:
            fa1_in = torch.ones_like(ref_obj.mask[slicer[1:]].squeeze()).unsqueeze(0) * ref_obj.fa
            fa2_in = torch.ones_like(ref_obj.mask[slicer[1:]].squeeze()).unsqueeze(0) * dyn_obj.fa
            input_image_list += [fa1_in, fa2_in]

        if 'TR' in self.inputs:
            tr_in = torch.ones_like(ref_obj.mask[slicer[1:]].squeeze()).unsqueeze(0) * ref_obj.tr
            input_image_list += [tr_in]

        return input_image_list

    def calculate_num_input_feats(self):
        number_features = 0
        if 'Ims' in self.inputs:
            number_features += 12

        if 'Scales' in self.inputs:
            number_features += 36

        if 'FAs' in self.inputs:
            number_features += 2

        if 'TR' in self.inputs:
            number_features += 1

        return number_features

    @staticmethod
    def get_globals(ref_obj, dyn_obj, slicer):

        global_image_list = [ref_obj.b0[slicer[1:]].squeeze().unsqueeze(0)]
        global_image_list += [ref_obj.phase[slicer][1:].squeeze() - ref_obj.phase[slicer][:-1].squeeze().squeeze()]
        global_image_list += [dyn_obj.phase[slicer][1:].squeeze() - dyn_obj.phase[slicer][:-1].squeeze()]
        global_image_list += [ref_obj.ims[slicer].squeeze(), dyn_obj.ims[slicer].squeeze()]

        return global_image_list

    def get_slice_object(self, shape):

        if self.threeD:
            # Need to cut down image size if 3d
            start_x = torch.LongTensor(1).random_(shape[1] - 128)
            start_y = torch.LongTensor(1).random_(shape[2] - 128)
            rand_slice = torch.LongTensor(1).random_(self.patch // 2, shape[self.dim + 1] - self.patch // 2)
            slicer_obj = [slice(shape[0]), slice(shape[1]), slice(shape[2]), slice(shape[3])]

            slicer_obj[1] = slice(start_x.item(), start_x.item() + 128)
            slicer_obj[2] = slice(start_y.item(), start_y.item() + 128)
            input_slicer = slicer_obj.copy()
            input_slicer[self.dim + 1] = slice(rand_slice.item() - self.patch // 2,
                                               rand_slice.item() + self.patch // 2 + 1)
            label_slicer = slicer_obj.copy()
            label_slicer[self.dim + 1] = slice(rand_slice.item(), rand_slice.item() + 1)

            return input_slicer, label_slicer

        else:
            rand_slice = torch.LongTensor(1).random_(shape[self.dim + 1])

            slicer_obj = [slice(shape[0]), slice(shape[1]), slice(shape[2]), slice(shape[3])]
            slicer_obj[self.dim + 1] = slice(rand_slice.item(), rand_slice.item() + 1)
            input_slicer = slicer_obj.copy()
            label_slicer = slicer_obj.copy()

            return input_slicer, label_slicer

    def __getitem__(self, item):
        # import matplotlib
        # matplotlib.use('qt5agg')
        # import matplotlib.pyplot as plt
        # plt.ion()

        index = item % len(self.objs)
        pair = self.objs[index]

        ims_shape = pair[0].ims.shape
        input_slicer, label_slicer = self.get_slice_object(ims_shape)

        # Get the label
        label = self.get_label(pair[0], label_slicer)
        mask = self.get_mask(pair[0], label_slicer)
        t1est_list = torch.cat(self.get_inputs(pair[0], pair[1], input_slicer), dim=0)
        global_list = torch.cat(self.get_globals(pair[0], pair[1], input_slicer), dim=0)

        # Spatially trasform the source and target
        # sing_t = torch.cat([t1est_list, global_list, mask.unsqueeze(0), label.unsqueeze(0)], dim=0)
        # sing_t = self.spatial(sing_t).squeeze()
        # t1est = sing_t[:len(t1est_list)]
        # scanner = sing_t[len(t1est_list): len(t1est_list) + len(global_list)]
        # mask = sing_t[-2].squeeze()
        # label = sing_t[-1].squeeze()

        return t1est_list.float(), global_list.float(), mask.bool(), label.float()

    def __len__(self):
        return self.length


class EvalDataset(data.Dataset):
    def __init__(self, objs, opt):
        super(EvalDataset, self).__init__()

        permutes = []
        for o in objs:
            ref_objs = [x for x in o if x.fa in opt.ref_angles]
            dyn_objs = [x for x in o if x.fa in opt.dyn_angles]
            permutes += list(product(ref_objs, dyn_objs))

        self.objs = copy.deepcopy(permutes)
        self.length = len(self.objs) * objs[0][0].ims.shape[-1]
        self.threeD = opt.threeD
        self.inputs = opt.inputs
        self.output = opt.label
        self.dim = 2
        self.patch = opt.patch

        if opt.threeD:
            self.length = len(objs) * (objs[0][0].ims.shape[-1] - (opt.patch - 1))

    def get_inputs(self, ref_obj, dyn_obj, slicer):

        mean_ims = torch.cat([ref_obj.mean[slicer], dyn_obj.mean[slicer]], dim=0).mean(dim=0).squeeze().unsqueeze(0)
        ref_ims = ref_obj.ims[slicer].squeeze()
        dyn_ims = dyn_obj.ims[slicer].squeeze()

        input_image_list = []
        if 'Ims' in self.inputs:
            b0 = ref_obj.b0[slicer[1:]].squeeze().unsqueeze(0)
            b0 = (b0 - b0.min()) / (b0.max() - b0.min())
            input_image_list += [ref_ims / mean_ims, dyn_ims / mean_ims, b0]

        if 'Scales' in self.inputs:
            sin1 = ref_ims / torch.sin(torch.tensor(ref_obj.fa) * (math.pi / 180))
            sin2 = dyn_ims / torch.sin(torch.tensor(dyn_obj.fa) * (math.pi / 180))
            tan1 = ref_ims / torch.tan(torch.tensor(ref_obj.fa) * (math.pi / 180))
            tan2 = dyn_ims / torch.tan(torch.tensor(dyn_obj.fa) * (math.pi / 180))
            div1 = sin1 / sin2
            div2 = sin2 / sin1
            log1 = torch.log(torch.clamp(torch.abs(tan2 - tan1), min=0.001))
            input_image_list += [div1, div2, log1, torch.log(sin1), torch.log(sin2)]

        if 'FAs' in self.inputs:
            fa1_in = torch.ones_like(ref_obj.mask[slicer[1:]].squeeze()).unsqueeze(0) * ref_obj.fa
            fa2_in = torch.ones_like(ref_obj.mask[slicer[1:]].squeeze()).unsqueeze(0) * dyn_obj.fa
            input_image_list += [fa1_in, fa2_in]

        if 'TR' in self.inputs:
            tr_in = torch.ones_like(ref_obj.mask[slicer[1:]].squeeze()).unsqueeze(0) * ref_obj.tr
            input_image_list += [tr_in]

        return input_image_list

    @staticmethod
    def get_mask(ref_obj, slicer):
        return ref_obj.mask[slicer[1:]].squeeze()

    def get_label(self, ref_obj, slicer):

        if self.output == 'T1' or self.output == 'B1':
            return torch.clamp(ref_obj.label, 0.001, 5.0)[slicer[1:]].squeeze()

        elif self.output == 'Slope':
            stir = torch.clamp(ref_obj.label, 0.001, 5.0)
            return (torch.exp(-ref_obj.tr / stir) ** 30)[slicer[1:]].squeeze()
        else:
            exit('Unknown Label Type. Please select from [T1, B1, Slope]')

    def get_slice_object(self, shape, index):

        if self.threeD:
            # Need to cut down image size if 3d
            slicer_obj = [slice(shape[0]), slice(shape[1]), slice(shape[2]), slice(shape[3])]

            input_slicer = slicer_obj.copy()
            input_slicer[self.dim + 1] = slice(index, index + self.patch)
            label_slicer = slicer_obj.copy()
            label_slicer[self.dim + 1] = slice(index + self.patch // 2, index + self.patch // 2 + 1)

            return input_slicer, label_slicer

        else:
            slicer_obj = [slice(shape[0]), slice(shape[1]), slice(shape[2]), slice(shape[3])]
            slicer_obj[self.dim + 1] = slice(index, index + 1)
            input_slicer = slicer_obj.copy()
            label_slicer = slicer_obj.copy()

            return input_slicer, label_slicer

    @staticmethod
    def get_globals(ref_obj, dyn_obj, slicer):

        global_image_list = [ref_obj.b0[slicer[1:]].squeeze().unsqueeze(0)]
        global_image_list += [ref_obj.phase[slicer][1:].squeeze() - ref_obj.phase[slicer][:-1].squeeze().squeeze()]
        global_image_list += [dyn_obj.phase[slicer][1:].squeeze() - dyn_obj.phase[slicer][:-1].squeeze()]
        global_image_list += [ref_obj.ims[slicer].squeeze(), dyn_obj.ims[slicer].squeeze()]

        return global_image_list

    def __getitem__(self, item):

        obj_idx = item % len(self.objs)
        sl = (item // len(self.objs)) % self.objs[0][0].ims.shape[-1]
        pair = self.objs[obj_idx]

        # Get the slicer objects
        ims_shape = pair[0].ims.shape
        input_slicer, label_slicer = self.get_slice_object(ims_shape, sl)

        # Get the label
        label = self.get_label(pair[0], label_slicer)
        mask = self.get_mask(pair[0], label_slicer)
        t1est_list = torch.cat(self.get_inputs(pair[0], pair[1], input_slicer), dim=0)
        global_list = torch.cat(self.get_globals(pair[0], pair[1], input_slicer), dim=0)

        return t1est_list.float(), global_list.float(), mask.bool(), label.float()

    def __len__(self):
        return self.length
