import os
import math
import torch
import glob
from itertools import permutations, product
import numpy as np
import argparse
import torch.nn as nn
import torch.optim as optim

import matplotlib
matplotlib.use('qt5agg')
import matplotlib.pyplot as plt
plt.ion()

from scipy.io import loadmat
from skimage.restoration import denoise_tv_chambolle
from skimage.feature import canny
from scipy.ndimage.morphology import *

import CAMP.Core as core
import CAMP.StructuredGridOperators as so
import CAMP.FileIO as io

parser = argparse.ArgumentParser(description='Multi-Flip Ange T1 Prediction Data Pre-Processing')

parser.add_argument('-d', '--data_path', type=str, default='/hdscratch/ucair/MFA_T1/Data/RawData3/',
                    help='Raw Data Path')
parser.add_argument('-o', '--out_path', type=str, default='./Data/PreProcessedData/FreshData/',
                    help='Path to save data')
parser.add_argument('-e', '--echos', type=int, default=6, help='Number of echos to include')
parser.add_argument('-r', '--ref_angles', type=float, default=[5, 10, 15, 20, 30, 40],
                    help='Angles to use as reference', nargs='*')
parser.add_argument('-dy', '--dyn_angles', type=float, default=[5, 10, 15, 20, 30, 40],
                    help='Angles to use as dynamic', nargs='*')

opt = parser.parse_args()


class FASample:
    def __init__(self, ims, phase, b0, label, mask, fa, tr):
        self.ims = ims
        self.mask = mask
        self.phase = phase
        self.label = label
        self.b0 = b0
        self.fa = fa
        self.tr = tr
        self.mean = ims.mean(0, keepdim=True)


def get_center_pos(d):

    try:
        pos = d['slicePos'][0, 0]
        if pos.size == 1:
            num_slice = 1
        else:
            num_slice = pos.shape[-1]
    except IndexError:
        raise Exception('Unknown dictionary for extracting center position')

    try:
        if num_slice == 1:
            cor_pos = [float(pos['dCor'])]
        else:
            cor_pos = np.concatenate(pos['dCor'][0]).squeeze().tolist()
    except ValueError:
        cor_pos = [0.0] * num_slice

    try:
        if num_slice == 1:
            tra_pos = [float(pos['dTra'])]
        else:
            tra_pos = np.concatenate(pos['dTra'][0]).squeeze().tolist()
    except ValueError:
        tra_pos = [0.0] * num_slice

    try:
        if num_slice == 1:
            sag_pos = [float(pos['dSag'])]
        else:
            sag_pos = np.concatenate(pos['dSag'][0]).squeeze().tolist()
    except ValueError:
        sag_pos = [0.0] * num_slice

    positions = []
    for p in range(0, num_slice):
        positions.append([sag_pos[p], tra_pos[p], cor_pos[p]])

    return positions


def get_resolution(d):

    try:
        resolution = d['res']
    except IndexError:
        raise Exception('Unknown dictionary for extracting resolution')

    while len(resolution) == 1:
        resolution = resolution[0]

    return resolution.tolist()


def phase_unwrap(phases, tes):
    from skimage.restoration import unwrap_phase

    def best_fit_line(x_values, y_values):
        m = (((x_values.mean(-1) * y_values.mean(-1)) - (x_values * y_values).mean(-1)) /
             ((x_values.mean(-1)) ** 2 - (x_values ** 2).mean(-1)))

        b = y_values.mean(-1) - m * x_values.mean(-1)

        return m, b

    # Need to unwrap the second echo in the spatial dimension - the first echo is going to be solved for
    phases[..., 1] = unwrap_phase(phases[..., 1])

    # Now use the unwrapped image to unwrap down the echo train
    # unwrap = np.expand_dims(phases[..., 1], -1) + \
    # np.cumsum(np.angle(phases[..., 2:] * np.conj(phases[..., 1:-1])), -1)
    # unwrapped = torch.tensor(np.concatenate([np.expand_dims(phases[..., 1], -1), unwrap], -1))
    unwrapped = torch.tensor(np.unwrap(phases[..., 1:], axis=-1))

    # Solve for the first echo phase image
    tes_vol = torch.tensor(tes, dtype=torch.float32).view(1, 1, 1, 6).repeat(unwrapped.shape[:-1] + (1,))
    m, b = best_fit_line(tes_vol[..., 1:], unwrapped)
    first_echo = m * tes_vol[..., 0] + b
    echo_phases = (torch.cat([first_echo.unsqueeze(-1), unwrapped], dim=-1) - b.unsqueeze(-1)) / (2 * math.pi)

    return echo_phases.numpy()


def get_sos_vols(sos_dicts):

    sos_data = []
    for i, d in enumerate(sos_dicts, 1):
        print(f'Processing FA {i}/{len(sos_dicts)} ... ', end='')
        data = d['imsROSOS'].squeeze()
        tes = d['param'][0, 0]['TE'][0][0:6]

        magnitude = np.abs(data) / 100000.0
        angle = np.angle(data)

        angle = phase_unwrap(angle, tes)
        b0 = np.mean((np.diff(angle) / (42.58 * 3 * np.diff(tes))), -1)

        center_pos = get_center_pos(d['param'][0, 0])
        spacing = get_resolution(d['param'][0, 0])
        origin = [
            center_pos[0][0] - ((data.shape[0] / 2) * spacing[0]),
            center_pos[0][1] - ((data.shape[1] / 2) * spacing[1]),
            center_pos[0][2] - ((data.shape[2] / 2) * spacing[2]),
        ]

        mag_vol = core.StructuredGrid(
            data.shape[0:3],
            spacing=spacing,
            origin=origin,
            tensor=torch.tensor(magnitude).permute(3, 0, 1, 2),
            channels=6
        )

        phs_vol = core.StructuredGrid(
            data.shape[0:3],
            spacing=spacing,
            origin=origin,
            tensor=torch.tensor(angle).permute(3, 0, 1, 2),
            channels=6
        )

        b0_vol = core.StructuredGrid(
            data.shape[0:3],
            spacing=spacing,
            origin=origin,
            tensor=torch.tensor(b0).unsqueeze(0),
            channels=1
        )

        mag_vol.fa = np.round(float(d['param'][0, 0]['FA'][0, 0]))
        mag_vol.tr = float(d['param'][0, 0]['TR'][0, 0])

        sos_data.append([mag_vol, phs_vol, b0_vol])
        print('done')

    # Sort the Sos data so that the low flip angle is always first
    sos_data = [sos_data[i] for i in np.argsort([x[0].fa for x in sos_data])]

    return sos_data


def get_mask(sos_vol):
    fa05_mask = sos_vol.data.squeeze().numpy()

    tv_denoise = denoise_tv_chambolle(fa05_mask, weight=0.1)
    mask = np.zeros_like(tv_denoise)

    for i in range(0, mask.shape[-1]):
        mask[:, :, i] = canny(tv_denoise[:, :, i], sigma=1.0, low_threshold=.001, high_threshold=0.3)
        mask[:, :, i] = binary_dilation(mask[:, :, i])
        mask[:, :, i] = binary_fill_holes(mask[:, :, i])
        mask[:, :, i] = binary_dilation(binary_erosion(binary_erosion(mask[:, :, i])))

    sos_mask = sos_vol.clone()
    sos_mask.data = torch.tensor(mask).unsqueeze(0)

    return sos_mask


def generate_stir_vol(stir_center, stir_space, stir_data):

    z_space = []
    for i, c in enumerate(stir_center[1:]):
        z_space.append(c[2] - stir_center[i][2])

    z_slab = np.round(np.array(z_space).mean(), 3)
    vol_org = [stir_center[0][0] - ((stir_data.shape[0] / 2) * stir_space[0]),
               stir_center[0][1] - ((stir_data.shape[1] / 2) * stir_space[1]),
               stir_center[0][2] - z_slab / 2.0]

    vol_space = [stir_space[0], stir_space[1], z_slab]

    stir_vol = core.StructuredGrid(
        [stir_data.shape[0], stir_data.shape[1], stir_data.shape[2]],
        spacing=vol_space,
        origin=vol_org,
        tensor=torch.tensor(stir_data).unsqueeze(0),
        channels=1
    )

    return stir_vol


def get_stir_vol(stir_dicts):

    datas = []
    centers = []
    resolutions = []

    try:
        for d in stir_dicts:
            centers += get_center_pos(d['param'][0, 0])
            resolutions.append(get_resolution(d['param'][0, 0]))
            datas.append(d['T1'] / 1000.0)  # Scale from mSeconds to Seconds
    except KeyError:
        for d in stir_dicts:
            for im in range(1, 3):
                centers += get_center_pos(d[f'param_{im}'][0, 0])
                resolutions.append(get_resolution(d[f'param_{im}'][0, 0]))
                datas.append(d[f'T1_{im}'] / 1000.0)

                # for d in stir_dicts:
    #     centers += get_center_pos(d['param'][0, 0])
    #     resolutions.append(get_resolution(d['param'][0, 0]))
    #     datas.append(d['T1'] / 1000.0)  # Scale from mSeconds to Seconds

    sorted_inds = np.argsort(np.array(centers)[:, -1])
    stacked_data = np.concatenate(datas, -1)

    sorted_centers = []
    sorted_data = np.zeros_like(stacked_data)
    for i, ind in enumerate(sorted_inds):
        sorted_centers.append(centers[ind])
        sorted_data[:, :, i] = stacked_data[:, :, ind]

    return generate_stir_vol(sorted_centers, resolutions[0], sorted_data)


def process_experiment(folder):

    print(f'Processing {folder} ... ')

    # Get a list of the files
    image_files = sorted(glob.glob(f'{folder}/*'))

    stir_files = [x for x in image_files if 'tuk_opt' in x]
    stir_dicts = [loadmat(x) for x in stir_files]

    stir_vol = get_stir_vol(stir_dicts)
    if folder.split('/')[-1] == '20_06_09':
        stir_vol.data = stir_vol.data.flip(-1)
    if folder.split('/')[-1] == '20_08_06':
        stir_vol.data = stir_vol.data.flip(-1)

    sos_files = [x for x in image_files if 'SOS' in x]
    sos_dicts = [loadmat(x) for x in sos_files]

    sos_data = get_sos_vols(sos_dicts)

    date = '-'.join(folder.split('/')[-1][2:10].split('_'))
    mask_files = [x for x in sorted(glob.glob(f'./Data/Segmentation/*')) if 'mask' in x and date in x]
    if len(mask_files) > 1:
        mask_files = [x for x in mask_files if folder.split('_')[-1] in x]

    try:
        mask = io.LoadITKFile(mask_files[0])
    except IOError:
        print('... no segmentation mask found ... ', end='')
        exit()

    # try:
    #     mask = io.LoadITKFile(f'./Data/Segmentation/{folder.split("/")[-1].split(".")[0]}_mask.nrrd')
    # except IOError:
    #     print('... no segmentation mask found ... ', end='')
    #     exit()

    segment = mask.clone()
    segment.data = sos_data[3][0].data[None, 0]
    io.SaveITKFile(segment, f'./Data/Segmentation/{folder.split("/")[-1]}_segment.nrrd')

    # Resample the STIR onto the SoS grid
    stir_resamp = so.ResampleWorld.Create(sos_data[0][-1])(stir_vol)

    nz_z = torch.LongTensor([i for i in range(0, stir_resamp.shape()[-1]) if mask[:, :, :, i].sum() != 0][1:-1])
    nz_y = torch.LongTensor([i for i in range(0, stir_resamp.shape()[-2]) if mask[:, :, i, :].sum() != 0][1:-1])
    nz_x = torch.LongTensor([i for i in range(0, stir_resamp.shape()[-3]) if mask[:, i, :, :].sum() != 0][1:-1])

    rs_z = 64
    rs_y = 224
    rs_x = 224

    stir_out = core.StructuredGrid(
        list(stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape)[1:],
        spacing=stir_resamp.spacing,
        origin=stir_resamp.origin,
        tensor=stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :],
        channels=stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape[0]
    ).set_size((rs_x, rs_y, rs_z), inplace=False)

    mask_out = core.StructuredGrid(
        list(stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape)[1:],
        spacing=stir_resamp.spacing,
        origin=stir_resamp.origin,
        tensor=mask[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :],
        channels=stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape[0]
    ).set_size((rs_x, rs_y, rs_z), inplace=False)

    if not os.path.exists(f'./Data/Volumes/{folder.split("/")[-1]}'):
        os.makedirs(f'./Data/Volumes/{folder.split("/")[-1]}')
    io.SaveITKFile(stir_out, f'./Data/Volumes/{folder.split("/")[-1]}/ims_stir.nrrd')
    io.SaveITKFile(mask_out, f'./Data/Volumes/{folder.split("/")[-1]}/mask.nrrd')

    fa_samples = []
    for i in range(0, len(sos_data)):

        mag_out = core.StructuredGrid(
            list(stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape)[1:],
            spacing=stir_resamp.spacing,
            origin=stir_resamp.origin,
            tensor=sos_data[i][0][:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :],
            channels=6
        ).set_size((rs_x, rs_y, rs_z), inplace=False)

        phs_out = core.StructuredGrid(
            list(stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape)[1:],
            spacing=stir_resamp.spacing,
            origin=stir_resamp.origin,
            tensor=sos_data[i][1][:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :],
            channels=6
        ).set_size((rs_x, rs_y, rs_z), inplace=False)

        b0_out = core.StructuredGrid(
            list(stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape)[1:],
            spacing=stir_resamp.spacing,
            origin=stir_resamp.origin,
            tensor=sos_data[i][2][:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :],
            channels=1
        ).set_size((rs_x, rs_y, rs_z), inplace=False)

        sng_mag = core.StructuredGrid(
            list(stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape)[1:],
            spacing=stir_resamp.spacing,
            origin=stir_resamp.origin,
            tensor=sos_data[i][0][:, :, :, nz_z][:, :, nz_y, :][0, nz_x, :, :].unsqueeze(0),
            channels=1
        ).set_size((rs_x, rs_y, rs_z), inplace=False)

        sng_phase = core.StructuredGrid(
            list(stir_resamp[:, :, :, nz_z][:, :, nz_y, :][:, nz_x, :, :].shape)[1:],
            spacing=stir_resamp.spacing,
            origin=stir_resamp.origin,
            tensor=sos_data[i][1][:, :, :, nz_z][:, :, nz_y, :][0, nz_x, :, :].unsqueeze(0),
            channels=1
        ).set_size((rs_x, rs_y, rs_z), inplace=False)

        io.SaveITKFile(sng_mag, f'./Data/Volumes/{folder.split("/")[-1]}/ims_fa{sos_data[i][0].fa}_echo1.nrrd')
        io.SaveITKFile(sng_phase, f'./Data/Volumes/{folder.split("/")[-1]}/phs_fa{sos_data[i][0].fa}_echo1.nrrd')
        io.SaveITKFile(b0_out, f'./Data/Volumes/{folder.split("/")[-1]}/b0_{sos_data[i][0].fa}.nrrd')

        fa_samples.append(
            FASample(mag_out.data.cpu().squeeze(),
                     phs_out.data.cpu().squeeze(),
                     b0_out.data.cpu().squeeze(),
                     stir_out.data.cpu().squeeze(),
                     mask_out.data.cpu().squeeze(),
                     sos_data[i][0].fa, sos_data[i][0].tr)
        )

    print(f'Processing {folder} ... done')
    return fa_samples


def process_files(opt):
    data_path = opt.data_path
    out_path = opt.out_path

    if not os.path.exists(out_path):
        os.makedirs(out_path)

    folders = sorted(glob.glob(f'{data_path}/*'))

    experiment_data = []
    for folder in folders:
        experiment_data.append(process_experiment(folder))

    testing_data = [experiment_data.pop(-1)]
    # validation_data = [experiment_data.pop(-1)]
    training_data = experiment_data

    print('Saving ... ', end='')
    torch.save(training_data, f'{out_path}/train_objects.pt')
    # torch.save(validation_data, f'{out_path}/infer_objects.pt')
    torch.save(testing_data, f'{out_path}/test_objects.pt')
    print('done')


if __name__ == '__main__':
    process_files(opt)
