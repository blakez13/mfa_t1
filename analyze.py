import os
import math
import torch
import glob
from itertools import permutations, product
import numpy as np
import argparse
import torch.nn as nn
import torch.optim as optim

import matplotlib
matplotlib.use('qt5agg')
import matplotlib.pyplot as plt
plt.ion()

from scipy.io import loadmat
from skimage.restoration import denoise_tv_chambolle
from skimage.feature import canny
from scipy.ndimage.morphology import *

import CAMP.Core as core
import CAMP.StructuredGridOperators as so
import CAMP.FileIO as io

parser = argparse.ArgumentParser(description='Multi-Flip Ange T1 Prediction Data Pre-Processing')

parser.add_argument('-d', '--dataDirectory', type=str, default='./Data/PreProcessedData/VolumeDataPhase/',
                    help='Raw Data Path')
parser.add_argument('-o', '--out_path', type=str, default='./Data/PreProcessedData/VolumeDataPhase/',
                    help='Path to save data')
parser.add_argument('-e', '--echos', type=int, default=6, help='Number of echos to include')
parser.add_argument('-r', '--ref_angles', type=float, default=[5, 10, 15, 20, 30, 40],
                    help='Angles to use as reference', nargs='*')
parser.add_argument('-dy', '--dyn_angles', type=float, default=[5, 10, 15, 20, 30, 40],
                    help='Angles to use as dynamic', nargs='*')

opt = parser.parse_args()


class FASample:
    def __init__(self, ims, phase, b0, label, mask, fa, tr):
        self.ims = ims
        self.mask = mask
        self.phase = phase
        self.label = label
        self.b0 = b0
        self.fa = fa
        self.tr = tr
        self.mean = ims.mean(0, keepdim=True)


def vial_data_comp(opt):
    slc_num = 31

    train_samples = torch.load(f'{opt.dataDirectory}/train_objects.pt')
    infer_samples = torch.load(f'{opt.dataDirectory}/infer_objects.pt')

    old_old_vials = train_samples[-2]
    old_vials = train_samples[-1]
    new_vials = infer_samples[0]

    # Load the vial masks
    oovm = io.LoadITKFile('./Data/Volumes/20-04-23_vials/medium_vial_mask_old.nrrd').data.squeeze().bool()
    ovm = io.LoadITKFile('./Data/Volumes/20-06-09_vialsBreastSystem/medium_vial_mask_old.nrrd').data.squeeze().bool()
    nvm = io.LoadITKFile('./Data/Volumes/20-08-06_VialsInBreastSystem/medium_vial_mask_new.nrrd').data.squeeze().bool()

    new_mean = new_vials[0].label[nvm].mean()
    old_mean = old_vials[0].label[ovm].mean()
    old_old_mean = old_old_vials[0].label[oovm].mean()

    # get the average image intensity across the 6 flip angles and the 6 echos
    new_image_mean = torch.zeros((6, 6))
    old_image_mean = torch.zeros((6, 6))
    old_old_image_mean = torch.zeros((6, 6))
    for fa in range(0, 6):
        for e in range(0, 6):
            new_image_mean[fa, e] = new_vials[fa].ims[e][nvm].mean()
            old_image_mean[fa, e] = old_vials[fa].ims[e][ovm].mean()
            old_old_image_mean[fa, e] = old_old_vials[fa].ims[e][oovm].mean()


    new_phase_mean = torch.zeros((6, 6))
    old_phase_mean = torch.zeros((6, 6))
    old_old_phase_mean = torch.zeros((6, 6))
    for fa in range(0, 6):
        for e in range(0, 6):
            new_phase_mean[fa, e] = new_vials[fa].phase[e][nvm].mean()
            old_phase_mean[fa, e] = old_vials[fa].phase[e][ovm].mean()
            old_old_phase_mean[fa, e] = old_old_vials[fa].phase[e][oovm].mean()

if __name__ == '__main__':
    vial_data_comp(opt)
