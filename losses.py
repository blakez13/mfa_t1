import math
import torch
import torch.nn as nn


class DICELoss(nn.Module):

    def __init__(self):
        super(DICELoss, self).__init__()

    @staticmethod
    def forward(probs, target, eps=1e-4):
        """
        probs is a torch variable of size 1 x nclasses x H x W representing log probabilities for each class
        target is a 1-hot (integer) representation of the groundtruth, shoud have same size as the input
        """

        probs += eps
        target += eps

        num = probs * target  # b,c,h,w--p*g
        num = num.sum(dim=(2, 3))

        den1 = probs * probs  # --p^2
        den1 = den1.sum(dim=(2, 3))

        den2 = target * target  # --g^2
        den2 = den2.sum(dim=(2, 3))

        dice = 2 * (num / (den1 + den2))

        dice_loss = 1 - torch.sum(dice) / dice.size(0)  # divide by batch_sz

        return dice_loss


class MFALoss(nn.Module):

    def __init__(self,):
        super(MFALoss, self).__init__()
        self.act = nn.Softsign()
        self.t_hold = nn.Threshold(0.001, 0.001)

    def forward(self, pred, extras, eps=1e-3):

        if len(pred.shape) == 3:
            pred = pred.unsqueeze(0)
            extras = extras.unsqueeze(0)

        if len(pred.shape) == 5:
            pred = pred.squeeze(-1)

        in1 = extras[:, 0:6]
        in2 = extras[:, 6:12]
        fas1 = extras[:, None, 12] * (math.pi / 180)
        fas2 = extras[:, None, 13] * (math.pi / 180)
        trs = extras[:, None, 14]
        pred = (self.act(pred) + 1) * 2

        num = (in2 / torch.sin(fas2 * pred)) - (in1 / torch.sin(fas1 * pred))
        den = (in2 / torch.tan(fas2 * pred)) - (in1 / torch.tan(fas1 * pred))

        den[(torch.abs(den) <= eps)] = eps
        num[(torch.abs(den) <= eps)] = eps

        slope = num / den
        slope = self.t_hold(slope)
        # slope[(slope >= 1.1)] = 1.0

        log_t = torch.log(slope)
        log_t[torch.abs(log_t) <= eps] = eps

        t1 = (-trs / log_t).mean(dim=1)
        return t1
